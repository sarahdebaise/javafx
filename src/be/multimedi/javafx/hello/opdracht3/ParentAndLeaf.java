package be.multimedi.javafx.hello.opdracht3;

//verkort p 56

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class ParentAndLeaf extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Label helloLabel = new Label("Hello World");
        Parent root =
                FXMLLoader.load(getClass().getResource("opdracht1en2/HelloWorld.fxml"));
        Scene scene = new Scene(root, 300,100);
        stage.setTitle("Hello World");
        stage.setScene(scene);
        stage.show();
    }
}
